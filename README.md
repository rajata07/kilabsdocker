# KI Labs Calendar Docker Setup

Please follow the below instructions to run the app locally:

## Setup Instructions 

* Clone the docker repo
 ```
  git clone https://rajata07@bitbucket.org/rajata07/kilabsdocker.git
```
  
* For first time setup, please run:
```
cd kilabsdocker
chmod +x setup.sh
./setup.sh --first
```
    
* Try application by accessing interactive documentation at 
```
   http://localhost:8001/api/documentation
```