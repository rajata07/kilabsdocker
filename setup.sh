#!/bin/sh

#Clone repo
if [ ! -f "./kicalenderapp/.git/config" ]; then
    echo "Cloning the repo..."
    git clone https://rajata07@bitbucket.org/rajata07/kicalenderapp.git
fi

#install vendor
if [ ! -d "./kicalenderapp/vendor/" ]; then
    echo "installing dependencies.."
    cd kicalenderapp && php composer.phar install
fi

if [ $# -ne 0 ] && [ $1 = "--rebuild" ]; then
    docker-compose build
fi

echo "Bringing up the containers..."
docker-compose up -d --force-recreate &&

if [ $# -ne 0 ] && [ $1 = "--first" ]; then
    docker exec -it ki-labs-calender php artisan migrate &&
    docker exec -it ki-labs-calender php artisan db:seed
fi

docker exec -it ki-labs-calender php artisan swagger-lume:generate
